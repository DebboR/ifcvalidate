﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Reflection;
using Xbim.Common;
using Xbim.Ifc;
using Xbim.Ifc2x3.Interfaces;
using Xbim.Common.Step21;
using Xbim.IO;

using IfcValidate.ReportNS;
using log4net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace IfcValidate {
    class Program {
        static void initLog4Net() {
            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));
            var repo = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(),
                       typeof(log4net.Repository.Hierarchy.Hierarchy));
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
        }

        static void Main(string[] args) {
            // Logger
            initLog4Net();
            ILog logger = log4net.LogManager.GetLogger("IfcValidate");

            // Handle arguments
            String inputPath = "";
            String reportOutputPath = "";
            String ifcOutputPath = "";
            switch(args.Length) {
                case 0:
                    logger.Warn("No file specified as argument. Default 'Ifc Schependomlaan' used.");
                    inputPath = Path.GetFullPath("IFC Schependomlaan.ifc");
                    reportOutputPath = Path.GetFullPath("report.json");
                    ifcOutputPath = Path.GetFullPath("output.ifc");
                    break;
                case 1:
                    inputPath = Path.GetFullPath(args[0]);
                    reportOutputPath = Path.GetFullPath(Path.GetFileNameWithoutExtension(inputPath) + "_report.json");
                    ifcOutputPath = Path.GetFullPath(Path.GetFileNameWithoutExtension(inputPath) + "_ifc.ifc");
                    break;
                case 3:
                    inputPath = Path.GetFullPath(args[0]);
                    reportOutputPath = Path.GetFullPath(args[1]);
                    ifcOutputPath = Path.GetFullPath(args[2]);
                    break;
                default:
                    logger.Error("Too many arguments!");
                    return;
            }

            // Main logic
            Report report;
            try {

                ReportProgressDelegate openProgressDelegate = new ReportProgressDelegate((int progressPercent, object o) => {
                    Console.Error.WriteLine(progressPercent);
                });

                using (var model = IfcStore.Open(inputPath, ifcDatabaseSizeThreshHold: 500, progDelegate: openProgressDelegate)) {
                    // Validate model
                    Validator.Validate(model);

                    // Convert to EXPRESS Ifc file
                    FileStream f = File.OpenWrite(ifcOutputPath);
                    model.SaveAsIfc(f);
                    f.Close();

                    // Get statistics
                    // Todo: Implement

                    // Create report
                    report = new Report(model, (Path.GetFullPath(inputPath)).StorageType()) {
                        ValidationResult = ValidationResult.Success
                    };
                }
            } catch (Exception e){
                // Create report with error
                report = new Report() {
                    FileName = inputPath,
                    ValidationResult = ValidationResult.ParseError,
                    Error = e.Message
                };
            }

            // Write report
            File.WriteAllText(reportOutputPath, JToken.Parse(report.GenerateJson()).ToString(Newtonsoft.Json.Formatting.Indented));

            #if DEBUG
                Console.WriteLine("Report: " + JToken.Parse(report.GenerateJson()).ToString(Newtonsoft.Json.Formatting.Indented));
                Console.Read();
            #endif
            Console.WriteLine("Finished validating");
        }
    }
}
