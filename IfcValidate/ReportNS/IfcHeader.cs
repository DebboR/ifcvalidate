﻿using System;
using System.Collections.Generic;
using System.Text;
using Xbim.Common;
using Xbim.Common.Step21;

namespace IfcValidate.ReportNS {
    class IfcHeader {
        // Ifc file_description
        public List<String> Description { get; set; }
        public String ImplementationLevel { get; set; }

        // Ifc file_name
        public String Name { get; set; }
        public DateTime TimeStamp { get; set; }
        public List<String> Authors { get; set; }
        public List<String> Organizations { get; set; }
        public String PreprocessorVersion { get; set; }
        public String OriginatingSystem { get; set; }
        public String Authorization { get; set; }

        // Ifc file_schema
        public List<String> SchemaIdentifiers { get; set; }


        // Constructors
        public IfcHeader() { }
        
        public IfcHeader(IModel model) {
            IStepFileHeader header = model.Header;

            this.Description = new List<String>(header.FileDescription.Description);
            this.ImplementationLevel = header.FileDescription.ImplementationLevel;
            this.Name = header.FileName.Name;
            this.TimeStamp = DateTime.Parse(header.FileName.TimeStamp);
            this.Authors = new List<String>(header.FileName.AuthorName);
            this.Organizations = new List<String>(header.FileName.Organization);
            this.PreprocessorVersion = header.FileName.PreprocessorVersion;
            this.OriginatingSystem = header.FileName.OriginatingSystem;
            this.Authorization = header.FileName.AuthorizationName;
            this.SchemaIdentifiers = new List<String>(header.FileSchema.Schemas);
        }
    }
}
