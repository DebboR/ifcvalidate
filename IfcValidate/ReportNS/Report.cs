﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Xbim.Ifc;
using Xbim.Common.Step21;
using Xbim.IO;
using Xbim.Common;
using System.IO;

namespace IfcValidate.ReportNS {
    class Report {
        // General information about the file
        public String FileName { get; set; }
        public IfcSchemaVersion SchemaVersion { get; set; }
        public IfcStorageType StorageType { get; set; }

        // Validation information
        public ValidationResult ValidationResult { get; set; }
        public String Error { get; set; }

        // Ifc header information
        public IfcHeader IfcHeader { get; set; }

        // Ifc statistics
        public IfcStatistics IfcStatistics { get; set; }

        // Constructor
        public Report() {}

        public Report(IfcStore model, IfcStorageType storageType) : this() {
            this.FileName = Path.GetFileNameWithoutExtension(model.FileName);
            this.SchemaVersion = model.IfcSchemaVersion;
            this.StorageType = storageType;

            this.IfcHeader = new IfcHeader(model);
            this.IfcStatistics = new IfcStatistics(model);
        }

        // Methods
        public String GenerateJson() {
            return JsonConvert.SerializeObject(this, new StringEnumConverter());
        }

    }
}
