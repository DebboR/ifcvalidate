﻿using System;
using System.Collections.Generic;
using System.Text;
using Xbim.Ifc;

namespace IfcValidate.ReportNS {
    class IfcStatistics {
        public long InstanceCount { get; set; }


        // Constructors
        public IfcStatistics() { }

        public IfcStatistics(IfcStore model) {
            this.InstanceCount = model.Instances.Count;
        }
    }
}
