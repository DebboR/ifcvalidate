﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IfcValidate.ReportNS {
    [Flags]
    public enum ValidationResult {
        Success = 1,
        ParseError = 2,
        ValidationError = 4
    }
}
